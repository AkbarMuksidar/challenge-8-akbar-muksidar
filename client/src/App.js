// import logo from './logo.svg';
import React, { Component } from "react"
import './App.css';
import Register from "./components/Register";
import Login from "./components/Login";
import Edit from "./components/Edit";

class App extends Component {
  state = {
    // showing: "",
  };

  handleHome = () => {
    this.setState({ showing: "" });
  };
  handleRegister = () => {
    this.setState({ showing: "register" });
  };
  handleLogin = () => {
    this.setState({ showing: "login" });
  };
  handleEdit = () => {
    this.setState({ showing: "edit" });
  };
  render() {
    return (
      <div className="App">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="container">
            <span class="navbar-brand" href="#">
              THE GAME
            </span>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse">
              <div class="navbar-nav">
                <span class="nav-link" onClick={this.handleHome}>Home</span>
                <span class="nav-link" onClick={this.handleRegister}>Register</span>
                <span class="nav-link" onClick={this.handleLogin}>Login</span>
                <span class="nav-link" onClick={this.handleEdit}>Edit</span>
              </div>
            </div>
          </div>
        </nav>
        <div class="show">
          {!this.state.showing && <h1>HOME</h1>}
          {this.state.showing === "register" && <Register/>}
          {this.state.showing === "login" && <Login/>}
          {this.state.showing === "edit" && <Edit/>}
        </div>
      </div>
    );
  }
}


export default App;
