import React, { Component } from "react";
import '../App.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { nemail: '', password: '', formSubmitted: false };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleSubmit(event) {
    this.setState({ formSubmitted: true });

    event.preventDefault();
  }

  render() {
    return (
      <div class="row justify-content-md-center show">
        {this.state.formSubmitted && <div class="col col-lg-6">
          <table class="table caption-top show">
            <caption>List of Login</caption>
            <thead>
              <tr>
                <th scope="col">Email</th>
                <th scope="col">Password</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.state.email}</td>
                <td>{this.state.password}</td>
              </tr>
            </tbody>
          </table>
        </div>}

        {!this.state.formSubmitted && <div class="col col-lg-6">
          <form onSubmit={this.handleSubmit}>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email address</label>
              <input type="email" class="form-control" value={this.state.email} onChange={this.handleEmailChange} />
            </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Password</label>
              <input type="password" class="form-control" value={this.state.password} onChange={this.handlePasswordChange} />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>}
      </div>
    );
  }
}

export default Login