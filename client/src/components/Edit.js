import React, { Component } from "react";
import '../App.css';

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = { nama: '', email: '', formSubmitted: false };

    this.handleNamaChange = this.handleNamaChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNamaChange(event) {
    this.setState({ nama: event.target.value });
  }
  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleSubmit(event) {
    this.setState({ formSubmitted: true });

    event.preventDefault();
  }

  render() {
    return (
      <div class="row justify-content-md-center show">
        {this.state.formSubmitted && <div class="col col-lg-6">
          <table class="table caption-top show">
            <caption>List of Edit</caption>
            <thead>
              <tr>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.state.nama}</td>
                <td>{this.state.email}</td>
              </tr>
            </tbody>
          </table>
        </div>}

        {!this.state.formSubmitted && <div class="col col-lg-6">
          <form onSubmit={this.handleSubmit}>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Nama</label>
              <input type="text" class="form-control" value={this.state.nama} onChange={this.handleNamaChange} />
            </div>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email address</label>
              <input type="email" class="form-control" value={this.state.email} onChange={this.handleEmailChange} />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>}
      </div>
    );
  }
}

export default Edit